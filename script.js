var LinkBuilder = function (text, num) {

  var div = document.createElement('div');
  div.id = text;
  document.body.appendChild(div);

  function getCallback(val) {
    var message = 'clicked ' + val;
    function cb() {
      alert(message);
    }
    return cb;
  }

  function createAnchorElement(textElement, loopVar) {
    var element = document.createElement('a');
    element.onclick = getCallback(loopVar);
    var textNode = document.createTextNode(textElement);
    element.appendChild(textNode);
    return element;
  }


  this.build = function () {
    var loopVar, element;
    var docfrag = document.createDocumentFragment();
    for (loopVar = 1; loopVar <= num; loopVar++) {
      element = createAnchorElement(text + loopVar, loopVar);
      docfrag.appendChild(element);
    }
    document.getElementById(text).appendChild(docfrag);
  };

};

var LB1 = new LinkBuilder('test', 5);
LB1.build();